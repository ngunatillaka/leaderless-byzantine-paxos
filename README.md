Replicated state is a common feature of large scale distributed systems. Such systems need to reach consensus despite failing hardware and communications links.

Lamport initiated the discovery of the Paxos family of algorithms which are designed to reach consensus under such circumstances. To ensure progress however, they all require one non-faulty leader.

In a 2011 two-page note [Lamport](http://research.microsoft.com/en-us/um/people/lamport/pubs/disc-leaderless-web.pdf) hinted at the possibility of replacing dependence on such a leader with dependence on a certain level of synchrony, just enough to implement a virtual leader. We expand on this note, proving that our Leaderless Byzantine Paxos specification refines a version of Byzantine Paxos where there is precisely one non-faulty leader.

The original specifications by Lamport can be found at [here](http://research.microsoft.com/en-us/um/people/lamport/tla/byzpaxos.html)

### Setup ###
+ Install the [TLA toolbox](http://research.microsoft.com/en-us/um/people/lamport/tla/toolbox.html)
+ Install the [TLA+ Proof System](https://tla.msr-inria.inria.fr/tlaps/content/Home.html) (TLAPS)