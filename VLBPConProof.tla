
--------------------------- MODULE VLBPConProof -----------------------------
(***************************************************************************)
(* This module specifies the Leaderless Byzantine Paxos algorithm--a       *)
(* version of Paxos where the leader is replaced by a synchronous          *)
(* Byzantine consensus algorithm as described in                           *)
(*                                                                         *)
(*    author = "Leslie Lamport",                                           *)
(*    title = "Brief Announcement: Leaderless Byzantine Paxos",            *)
(*    booktitle = Proceedings of the 25th International Conference on      *)
(*                Distributed Computing,                                   *)
(*    year = 2011,                                                         *)
(*    pages = "141--142"                                                   *)
(***************************************************************************)

EXTENDS Integers, FiniteSets
----------------------------------------------------------------------------
CONSTANT Value

Ballot == Nat
None == CHOOSE v : v \notin Value
-----------------------------------------------------------------------------  
CONSTANTS Acceptor,       \* The set of good (non-faulty) acceptors.
          FakeAcceptor,   \* The set of possibly malicious (faulty) acceptors.
          ByzQuorum,     
          WeakQuorum     
 ByzAcceptor == Acceptor \cup FakeAcceptor

ASSUME BallotAssump == (Ballot \cup {-1}) \cap ByzAcceptor = {}

ASSUME BQA == 
          /\ Acceptor \cap FakeAcceptor = {}
          /\ \A Q \in ByzQuorum : Q \subseteq ByzAcceptor
          /\ \A Q1, Q2 \in ByzQuorum : Q1 \cap Q2 \cap Acceptor # {}
          /\ \A Q \in WeakQuorum : /\ Q \subseteq ByzAcceptor
                                   /\ Q \cap Acceptor # {}

ASSUME BQLA == 
          /\ \E Q \in ByzQuorum : Q \subseteq Acceptor 
          /\ \E Q \in WeakQuorum : Q \subseteq Acceptor 
-----------------------------------------------------------------------------
1aMessage == [type : {"1a"},  bal : Ballot]
  
1bMessage == 
  [type : {"1b"}, bal : Ballot, 
   mbal : Ballot \cup {-1}, mval : Value \cup {None},
   m2av : SUBSET [val : Value, bal : Ballot],
   acc : ByzAcceptor]

1cMessage == 
  [type : {"1c"}, bal : Ballot, val : Value] 

2avMessage ==
   [type : {"2av"}, bal : Ballot, val : Value, acc : ByzAcceptor]

2bMessage == [type : {"2b"}, acc : ByzAcceptor, bal : Ballot, val : Value]

BMessage == 
  1aMessage \cup 1bMessage \cup 1cMessage \cup 2avMessage \cup 2bMessage

LEMMA BMessageLemma ==
         \A m \in BMessage :
           /\ (m \in 1aMessage) <=>  (m.type = "1a")
           /\ (m \in 1bMessage) <=>  (m.type = "1b")
           /\ (m \in 1cMessage) <=>  (m.type = "1c")
           /\ (m \in 2avMessage) <=>  (m.type = "2av")
           /\ (m \in 2bMessage) <=>  (m.type = "2b")
<1>1. /\ \A m \in 1aMessage : m.type = "1a"
      /\ \A m \in 1bMessage : m.type = "1b"
      /\ \A m \in 1cMessage : m.type = "1c"
      /\ \A m \in 2avMessage : m.type = "2av"
      /\ \A m \in 2bMessage : m.type = "2b"
  BY DEF 1aMessage, 1bMessage, 1cMessage, 2avMessage, 2bMessage
<1>2. QED
  BY <1>1 DEF BMessage             
-----------------------------------------------------------------------------

 (**************************************************************************

--algorithm VLBPCon {

  (**************************************************************************
The variables:

    maxBal[a]  = Highest ballot in which acceptor a has participated.

    maxVBal[a] = Highest ballot in which acceptor a has cast a vote
                 (sent a 2b message); or -1 if it hasn't cast a vote.

    maxVVal[a] = Value acceptor a has voted for in ballot maxVBal[a],
                  or None if maxVBal[a] = -1.

    2avSent[a] = A set of records in [val : Value, bal : Ballot] 
                 describing the 2av messages that a has sent.  A
                 record is added to this set, and any element with
                 a the same val field (and lower bal field) removed 
                 when a sends a 2av message.

    knownSent[a] = The set of 1b messages that acceptor a knows have
                   been sent.

    vlbmsgs = The set of all messages that have been sent.  See the
              discussion of the msgs variable in module PConProof
              to understand our modeling of message passing.

    holdingRoomInterest[a]  = -1 if the acceptor is uniterested in
         the holding room, 0 if it is waiting to enter and 1 if it
         is in the room.

    holdingRoomOpen = 0 is it is closed and 1 if it is open. 

    nextAction[a] = Next message by the virtual leader. 
    
    actionPerformed[a] = 0 if action not performed, 1 if performed.
    
    nextExpectedAction[a] = The message that 'a' believes should be sent by
         the virtual leader.
         
    VLVotingPhase[a] = The voting phase that the acceptor is in.

    VLVotingMessageInbox[a] = The voting messages received by the acceptor.

    VLVotingMessageStorage[a]  = The voting messages that an acceptor has 
         broadcasted.

    VLVotingCounter[a] is an array which each acceptor uses to count its votes.

    VLActionHistory = The messages sent by the virtual leader.
    
  **************************************************************************)
  variables maxBal  = [a \in Acceptor |-> -1],
            maxVBal = [a \in Acceptor |-> -1] ,
            maxVVal = [a \in Acceptor |-> None] ,
            2avSent = [a \in Acceptor |-> {}],
            knowsSent = [a \in Acceptor |-> {}],
            vlbmsgs = {}, 
            holdingRoomInterest = [a \in Acceptor |-> -1], 
            holdingRoomOpen = 1, (*0 = no, 1 = yes*)
            nextAction = [a \in Acceptor |-> [type |-> "None"]],
            actionPerformed = [a \in Acceptor |-> 0], 
            nextExpectedAction = [a \in Acceptor |-> [type |-> "None"]],
            VLVotingPhase = [a \in Acceptor |-> 0],
            VLVotingMessageInbox = [a \in ByzAcceptor |-> {}],
            VLVotingMessageStorage = [a \in ByzAcceptor |-> {}],
            VLVotingCounter = [a \in Acceptor |-> {}],
            VLActionHistory = {}
            
  define {
    sentMsgs(type, bal) == {m \in vlbmsgs: m.type = type /\ m.bal = bal}
    
    KnowsSafeAt(ac, b, v) ==
      LET S == {m \in knowsSent[ac] : m.bal = b}
      IN  \/ \E BQ \in ByzQuorum : 
               \A a \in BQ : \E m \in S : /\ m.acc = a 
                                          /\ m.mbal = -1
          \/ \E c \in 0..(b-1):
               /\ \E BQ \in ByzQuorum : 
                    \A a \in BQ : \E m \in S : /\ m.acc = a
                                               /\ m.mbal =< c
                                               /\ (m.mbal = c) => (m.mval = v)
               /\ \E WQ \in WeakQuorum :
                    \A a \in WQ : 
                      \E m \in S : /\ m.acc = a
                                   /\ \E r \in m.m2av : /\ r.bal >= c
                                                        /\ r.val = v
   } 

  (*************************************************************************)
  (* We now describe the processes' actions as macros.                     *)
  (*                                                                       *)
  (* The synchronizer signals the acceptors to start by setting their      *)
  (* nextAction for the first time                                         *)
  (*************************************************************************)

   macro SetInitialAction(){
    when \A a \in Acceptor: nextAction[a].type = "None";
    nextAction := [a \in Acceptor |-> [type |-> "1a", bal |-> 0]]; 
    VLActionHistory := VLActionHistory \cup {[type |-> "1a", bal |-> 0]};
   }

  (*************************************************************************)
  (* The acceptor's Phase1b, Phase2av and Phase2b actions are similar to   *)
  (* that of the Byzantine Paxos Consensus algorithm.                      *)
  (*************************************************************************)
  macro Phase1b(b) {
   when /\ actionPerformed[self] = 0
        /\ nextAction[self].type = "1a"
        /\ nextAction[self].bal = b      
        /\ b > maxBal[self];
   maxBal[self] := b ;
   vlbmsgs := vlbmsgs \cup {[type  |-> "1b", bal |-> b, acc |-> self,
                         m2av |-> 2avSent[self],
                         mbal |-> maxVBal[self], mval |-> maxVVal[self]]};
   actionPerformed[self] := 1;
   }

  macro Phase1bFailed(b) {
   when /\ actionPerformed[self] = 0
        /\ nextAction[self].type = "1a"
        /\ nextAction[self].bal = b      
        /\ ~(b > maxBal[self]);
   actionPerformed[self] := 1;
   }

  macro Phase2av(b) {
    when /\ actionPerformed[self] = 0
         /\ nextAction[self].type = "1c"
         /\ nextAction[self].bal = b 
         /\ maxBal[self] =< b  
            \* We could just as well have used r.bal # b in this condition.
         /\ \A r \in 2avSent[self] : r.bal < b
         /\ KnowsSafeAt(self, b, nextAction[self].val);
    with (m \in {nextAction[self]}) {
       vlbmsgs := vlbmsgs \cup 
                 {[type |-> "2av", bal |-> b, val |-> m.val, acc |-> self]};
       2avSent[self] :=  {r \in 2avSent[self] : r.val # m.val} 
                           \cup {[val |-> m.val, bal |-> b]}
      } ;
    maxBal[self]  := b ;
    actionPerformed[self] := 1;
   }

  macro Phase2avFailed(b) {
    when /\ actionPerformed[self] = 0
         /\ nextAction[self].type = "1c"
         /\ nextAction[self].bal = b 
         /\ ~(maxBal[self] =< b  
                  /\ \A r \in 2avSent[self] : r.bal < b
                  /\ KnowsSafeAt(self, b, nextAction[self].val));
    actionPerformed[self] := 1;
   }

  macro Phase2b(b) {
    when maxBal[self] =< b ;
    with (v \in {vv \in Value : 
                   \E Q \in ByzQuorum :
                      \A aa \in Q : 
                         \E m \in sentMsgs("2av", b) : /\ m.val = vv
                                                       /\ m.acc = aa} ) {
        vlbmsgs := vlbmsgs \cup 
                  {[type |-> "2b", acc |-> self, bal |-> b, val |-> v]} ;
        maxVVal[self] := v ;
      } ;
    maxBal[self] := b ;
    maxVBal[self] := b
   }
  
  macro LearnsSent(b) {
    with (S \in SUBSET sentMsgs("1b", b)) {
       knowsSent[self] := knowsSent[self] \cup S
     }
   }

  macro FakingAcceptor() {
    with ( m \in { mm \in 1bMessage \cup 2avMessage \cup 2bMessage : 
                   mm.acc = self} ) {
         vlbmsgs := vlbmsgs \cup {m}
     }
   }

  (*************************************************************************)
  (* A malicious acceptor `self' can now send messages to interfere with   *)
  (* the virtual leader voting.                                            *)
  (*************************************************************************)
   
   macro FakingAcceptorVotes() {
        with (a \in Acceptor;
              m \in {mm \in 1aMessage \cup 1cMessage :
                   /\ (mm \in 1aMessage => mm.bal = maxBal[a] + 1) 
                   /\ (mm \in 1cMessage => mm.bal = maxBal[a])}) {
            VLVotingMessageInbox[a] := 
                 VLVotingMessageInbox[a] \cup {[acc |-> self, vote |-> m ]};
        }
   }
   
     
  (*************************************************************************)
  (* Macros for synchronisation                                            *)
  (*************************************************************************)
   
  macro SetNextExpectedActionAndSynchronise(){
    when /\ actionPerformed[self] = 1 
         /\ VLVotingPhase[self] = 0 
         /\ holdingRoomInterest[self] = -1;
    with (v \in {vv \in 1aMessage \cup 1cMessage :
         /\ (nextAction[self].type = "1c" => 
              (vv.bal = maxBal[self] + 1 /\ vv.type = "1a"))
         /\ (nextAction[self].type = "1a" =>
              (/\ ((\E m \in sentMsgs("1b", nextAction[self].bal) : 
                   m.acc = self) => 
                        /\ ((\E val \in Value : 
                             KnowsSafeAt(self, nextAction[self].bal, val)) =>
                                  /\ vv.type = "1c"
                                  /\ KnowsSafeAt(self,
                                                 nextAction[self].bal,
                                                 vv.val)
                                  /\ vv.bal = nextAction[self].bal)
                        /\ ((\A val \in Value : 
                             ~KnowsSafeAt(self, nextAction[self].bal, vv.val)) =>
                                  (vv.bal = maxBal[self] + 1 /\ vv.type = "1a"))
                             )
               /\ ((\A m \in sentMsgs("1b", nextAction[self].bal) : 
                    m.acc # self) => 
                         (vv.bal = maxBal[self] + 1 /\ vv.type = "1a"))                    
                    ))
            }){
       nextExpectedAction[self] := v;
    };
    holdingRoomInterest[self] := 0; 
    VLVotingPhase[self] := 1;
    (* Initialise Variables *)
    VLVotingMessageInbox[self] := {};
    VLVotingMessageStorage[self] := {};
    VLVotingCounter[self] := {}
  }
   
  macro EnterHoldingRoom(){
    when holdingRoomOpen = 1 /\ holdingRoomInterest[self] = 0;
    holdingRoomInterest[self] := 1;
  }
  
  macro ExitHoldingRoom(){
    when holdingRoomOpen = 0 /\ holdingRoomInterest[self] = 1;
    holdingRoomInterest[self] := -1;
  }
  
  macro OpenHoldingRoom() {
    when /\ holdingRoomOpen = 0
         /\ \A a \in Acceptor : holdingRoomInterest[a] # 1;
    holdingRoomOpen := 1;
  }

  macro CloseHoldingRoom() {
    when /\ holdingRoomOpen = 1
         /\ \A a \in Acceptor : holdingRoomInterest[a] = 1;
    holdingRoomOpen := 0;
  }

  (*************************************************************************)
  (* Macros for voting for the virtual leaders action                      *)  
  (*************************************************************************)

  macro VLVotingPhase1(){
    when /\ VLVotingPhase[self] = 1 
         /\ holdingRoomInterest[self] = -1;
    with (newInbox \in {[a \in ByzAcceptor |-> 
             (VLVotingMessageInbox[a] \cup 
                  {[acc |-> self, vote |-> nextExpectedAction[self]]})]} ) {
        VLVotingMessageInbox := newInbox;
        VLVotingPhase[self] := 2;
        holdingRoomInterest[self] := 0;
    }
  }

  macro VLVotingPhase2StoreAndRelay(){
    when /\ VLVotingPhase[self] = 2
         /\ holdingRoomInterest[self] = -1
         /\ VLVotingMessageInbox[self] # {};
    with (newInbox \in 
              {[a \in ByzAcceptor |-> 
                   (VLVotingMessageInbox[a] \cup
                    VLVotingMessageInbox[self])]}) {  
        VLVotingMessageStorage[self] := VLVotingMessageStorage[self] \cup 
                                        VLVotingMessageInbox[self];
        VLVotingMessageInbox := newInbox;
    }
  }

  macro VLVotingPhase2CleanInbox(){
    when /\ VLVotingPhase[self] = 2
         /\ holdingRoomInterest[self] = -1;
    with (m \in VLVotingMessageInbox[self] \intersect 
                VLVotingMessageStorage[self]) {
        VLVotingMessageInbox[self] := VLVotingMessageInbox[self] \ {m};
    }
  }

  macro ForceExitPhase2() {
    when /\ \A a \in Acceptor: VLVotingPhase[a] = 2
         /\ \A a \in Acceptor: VLVotingMessageInbox[a] = {};
    VLVotingPhase := [a \in Acceptor |-> 3];
  }
  
  macro VLVotingPhase3AddCounter() {
    when /\ VLVotingPhase[self] = 3
         /\ holdingRoomInterest[self] = -1;
    with (m \in
              {mm \in VLVotingMessageStorage[self] :
                   /\ \A n \in VLVotingMessageStorage[self] :
                        mm.vote = n.vote \/ mm.acc # n.acc
                   /\ \A v \in VLVotingCounter[self] : v.vote # mm.vote}) {
        VLVotingMessageStorage[self] := VLVotingMessageStorage[self] \ {m};
        VLVotingCounter[self] := VLVotingCounter[self] \cup
                                 {[vote |-> m.vote, count |-> 1]};          
    }
  }
  
  macro VLVotingPhase3IncCounter() {
    when /\ VLVotingPhase[self] = 3
         /\ holdingRoomInterest[self] = -1;
    with (m \in
              {mm \in VLVotingMessageStorage[self] :
                   \A n \in VLVotingMessageStorage[self] :
                        mm.vote = n.vote \/ mm.acc # n.acc};
          v \in {vv \in VLVotingCounter[self] : vv.vote = m.vote}) {
        VLVotingMessageStorage[self] := VLVotingMessageStorage[self] \ {m};
        VLVotingCounter[self] := (VLVotingCounter[self] \ {v}) \cup 
                                 {[vote |-> v.vote, count |-> v.count + 1]};
    }
  }  
  
  macro VLVotingPhase3SetNextAction() {
    when /\ VLVotingPhase[self] = 3
         /\ holdingRoomInterest[self] = -1
         /\ \A m \in VLVotingMessageStorage[self]: 
              \E n \in VLVotingMessageStorage[self]:
                   n.acc = m.acc /\ n.vote # m.vote;
    with (m \in 
              {mm \in VLVotingCounter[self]:
                   \A n \in VLVotingCounter[self] : 
                        mm.count >= n.count /\
                       (mm.count = n.count => 
                            (mm.vote.bal <= n.vote.bal /\
                            (mm.vote.bal = n.vote.bal => 
                                 (mm.vote.type = "1c" /\ 
                                 (n.vote.type = "1c" => 
                                      mm.vote.val <= n.vote.val)))))}) {
      nextAction[self] := m.vote;
      nextExpectedAction[self] := [type |-> "None"];
      VLActionHistory := VLActionHistory \cup {m.vote};
      actionPerformed[self] := 0;
      VLVotingPhase[self] := 0;                                                 
    }
  }
  
  (*************************************************************************)
  (* We combine these individual actions into a complete algorithm in the  *)
  (* usual way, with separate process declarations for the acceptor,       *)
  (* leader, and fake acceptor processes.                                  *)
  (*************************************************************************)
  process (acceptor \in Acceptor) {
    acc: while (TRUE) { 
           either 
           with (b \in Ballot) {either Phase1b(b) or Phase1bFailed(b) 
                                  or Phase2av(b) or Phase2avFailed(b) 
                                  or Phase2b(b) or LearnsSent(b)}
           or SetNextExpectedActionAndSynchronise()
           or EnterHoldingRoom()
           or ExitHoldingRoom()
           or VLVotingPhase1()
           or VLVotingPhase2StoreAndRelay()           
           or VLVotingPhase2CleanInbox()
           or VLVotingPhase3AddCounter()
           or VLVotingPhase3IncCounter()
           or VLVotingPhase3SetNextAction()
    }
   }
   
   process (sync = 1){
      sync: while (TRUE) {
        either SetInitialAction()
        or OpenHoldingRoom()
        or CloseHoldingRoom()
        or ForceExitPhase2()
      }
   }

  process (facceptor \in FakeAcceptor) {
     facc : while (TRUE) { either FakingAcceptor() 
                               or FakingAcceptorVotes()}
   }
}

Below is the TLA+ translation, as produced by the translator.  (Some
blank lines have been removed.)
**************************************************************************)
\* BEGIN TRANSLATION
\* Label sync of process sync at line 460 col 13 changed to sync_
VARIABLES maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, 
          holdingRoomInterest, holdingRoomOpen, nextAction, actionPerformed, 
          nextExpectedAction, VLVotingPhase, VLVotingMessageInbox, 
          VLVotingMessageStorage, VLVotingCounter, VLActionHistory

(* define statement *)
sentMsgs(type, bal) == {m \in vlbmsgs: m.type = type /\ m.bal = bal}

KnowsSafeAt(ac, b, v) ==
  LET S == {m \in knowsSent[ac] : m.bal = b}
  IN  \/ \E BQ \in ByzQuorum :
           \A a \in BQ : \E m \in S : /\ m.acc = a
                                      /\ m.mbal = -1
      \/ \E c \in 0..(b-1):
           /\ \E BQ \in ByzQuorum :
                \A a \in BQ : \E m \in S : /\ m.acc = a
                                           /\ m.mbal =< c
                                           /\ (m.mbal = c) => (m.mval = v)
           /\ \E WQ \in WeakQuorum :
                \A a \in WQ :
                  \E m \in S : /\ m.acc = a
                               /\ \E r \in m.m2av : /\ r.bal >= c
                                                    /\ r.val = v


vars == << maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, 
           holdingRoomInterest, holdingRoomOpen, nextAction, actionPerformed, 
           nextExpectedAction, VLVotingPhase, VLVotingMessageInbox, 
           VLVotingMessageStorage, VLVotingCounter, VLActionHistory >>

ProcSet == (Acceptor) \cup {1} \cup (FakeAcceptor)

Init == (* Global variables *)
        /\ maxBal = [a \in Acceptor |-> -1]
        /\ maxVBal = [a \in Acceptor |-> -1]
        /\ maxVVal = [a \in Acceptor |-> None]
        /\ 2avSent = [a \in Acceptor |-> {}]
        /\ knowsSent = [a \in Acceptor |-> {}]
        /\ vlbmsgs = {}
        /\ holdingRoomInterest = [a \in Acceptor |-> -1]
        /\ holdingRoomOpen = 1
        /\ nextAction = [a \in Acceptor |-> [type |-> "None"]]
        /\ actionPerformed = [a \in Acceptor |-> 0]
        /\ nextExpectedAction = [a \in Acceptor |-> [type |-> "None"]]
        /\ VLVotingPhase = [a \in Acceptor |-> 0]
        /\ VLVotingMessageInbox = [a \in ByzAcceptor |-> {}]
        /\ VLVotingMessageStorage = [a \in ByzAcceptor |-> {}]
        /\ VLVotingCounter = [a \in Acceptor |-> {}]
        /\ VLActionHistory = {}

acceptor(self) == /\ \/ /\ \E b \in Ballot:
                             \/ /\ /\ actionPerformed[self] = 0
                                   /\ nextAction[self].type = "1a"
                                   /\ nextAction[self].bal = b
                                   /\ b > maxBal[self]
                                /\ maxBal' = [maxBal EXCEPT ![self] = b]
                                /\ vlbmsgs' = (vlbmsgs \cup {[type  |-> "1b", bal |-> b, acc |-> self,
                                                          m2av |-> 2avSent[self],
                                                          mbal |-> maxVBal[self], mval |-> maxVVal[self]]})
                                /\ actionPerformed' = [actionPerformed EXCEPT ![self] = 1]
                                /\ UNCHANGED <<maxVBal, maxVVal, 2avSent, knowsSent>>
                             \/ /\ /\ actionPerformed[self] = 0
                                   /\ nextAction[self].type = "1a"
                                   /\ nextAction[self].bal = b
                                   /\ ~(b > maxBal[self])
                                /\ actionPerformed' = [actionPerformed EXCEPT ![self] = 1]
                                /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs>>
                             \/ /\ /\ actionPerformed[self] = 0
                                   /\ nextAction[self].type = "1c"
                                   /\ nextAction[self].bal = b
                                   /\ maxBal[self] =< b
                                   
                                   /\ \A r \in 2avSent[self] : r.bal < b
                                   /\ KnowsSafeAt(self, b, nextAction[self].val)
                                /\ \E m \in {nextAction[self]}:
                                     /\ vlbmsgs' = ( vlbmsgs \cup
                                                    {[type |-> "2av", bal |-> b, val |-> m.val, acc |-> self]})
                                     /\ 2avSent' = [2avSent EXCEPT ![self] = {r \in 2avSent[self] : r.val # m.val}
                                                                               \cup {[val |-> m.val, bal |-> b]}]
                                /\ maxBal' = [maxBal EXCEPT ![self] = b]
                                /\ actionPerformed' = [actionPerformed EXCEPT ![self] = 1]
                                /\ UNCHANGED <<maxVBal, maxVVal, knowsSent>>
                             \/ /\ /\ actionPerformed[self] = 0
                                   /\ nextAction[self].type = "1c"
                                   /\ nextAction[self].bal = b
                                   /\ ~(maxBal[self] =< b
                                            /\ \A r \in 2avSent[self] : r.bal < b
                                            /\ KnowsSafeAt(self, b, nextAction[self].val))
                                /\ actionPerformed' = [actionPerformed EXCEPT ![self] = 1]
                                /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs>>
                             \/ /\ maxBal[self] =< b
                                /\ \E v \in {vv \in Value :
                                               \E Q \in ByzQuorum :
                                                  \A aa \in Q :
                                                     \E m \in sentMsgs("2av", b) : /\ m.val = vv
                                                                                   /\ m.acc = aa}:
                                     /\ vlbmsgs' = ( vlbmsgs \cup
                                                    {[type |-> "2b", acc |-> self, bal |-> b, val |-> v]})
                                     /\ maxVVal' = [maxVVal EXCEPT ![self] = v]
                                /\ maxBal' = [maxBal EXCEPT ![self] = b]
                                /\ maxVBal' = [maxVBal EXCEPT ![self] = b]
                                /\ UNCHANGED <<2avSent, knowsSent, actionPerformed>>
                             \/ /\ \E S \in SUBSET sentMsgs("1b", b):
                                     knowsSent' = [knowsSent EXCEPT ![self] = knowsSent[self] \cup S]
                                /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, vlbmsgs, actionPerformed>>
                        /\ UNCHANGED <<holdingRoomInterest, nextAction, nextExpectedAction, VLVotingPhase, VLVotingMessageInbox, VLVotingMessageStorage, VLVotingCounter, VLActionHistory>>
                     \/ /\ /\ actionPerformed[self] = 1
                           /\ VLVotingPhase[self] = 0
                           /\ holdingRoomInterest[self] = -1
                        /\ \E v \in        {vv \in 1aMessage \cup 1cMessage :
                                    /\ (nextAction[self].type = "1c" =>
                                         (vv.bal = maxBal[self] + 1 /\ vv.type = "1a"))
                                    /\ (nextAction[self].type = "1a" =>
                                         (/\ ((\E m \in sentMsgs("1b", nextAction[self].bal) :
                                              m.acc = self) =>
                                                   /\ ((\E val \in Value :
                                                        KnowsSafeAt(self, nextAction[self].bal, val)) =>
                                                             /\ vv.type = "1c"
                                                             /\ KnowsSafeAt(self,
                                                                            nextAction[self].bal,
                                                                            vv.val)
                                                             /\ vv.bal = nextAction[self].bal)
                                                   /\ ((\A val \in Value :
                                                        ~KnowsSafeAt(self, nextAction[self].bal, vv.val)) =>
                                                             (vv.bal = maxBal[self] + 1 /\ vv.type = "1a"))
                                                        )
                                          /\ ((\A m \in sentMsgs("1b", nextAction[self].bal) :
                                               m.acc # self) =>
                                                    (vv.bal = maxBal[self] + 1 /\ vv.type = "1a"))
                                               ))
                                       }:
                             nextExpectedAction' = [nextExpectedAction EXCEPT ![self] = v]
                        /\ holdingRoomInterest' = [holdingRoomInterest EXCEPT ![self] = 0]
                        /\ VLVotingPhase' = [VLVotingPhase EXCEPT ![self] = 1]
                        /\ VLVotingMessageInbox' = [VLVotingMessageInbox EXCEPT ![self] = {}]
                        /\ VLVotingMessageStorage' = [VLVotingMessageStorage EXCEPT ![self] = {}]
                        /\ VLVotingCounter' = [VLVotingCounter EXCEPT ![self] = {}]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, nextAction, actionPerformed, VLActionHistory>>
                     \/ /\ holdingRoomOpen = 1 /\ holdingRoomInterest[self] = 0
                        /\ holdingRoomInterest' = [holdingRoomInterest EXCEPT ![self] = 1]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, nextAction, actionPerformed, nextExpectedAction, VLVotingPhase, VLVotingMessageInbox, VLVotingMessageStorage, VLVotingCounter, VLActionHistory>>
                     \/ /\ holdingRoomOpen = 0 /\ holdingRoomInterest[self] = 1
                        /\ holdingRoomInterest' = [holdingRoomInterest EXCEPT ![self] = -1]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, nextAction, actionPerformed, nextExpectedAction, VLVotingPhase, VLVotingMessageInbox, VLVotingMessageStorage, VLVotingCounter, VLActionHistory>>
                     \/ /\ /\ VLVotingPhase[self] = 1
                           /\ holdingRoomInterest[self] = -1
                        /\ \E newInbox \in           {[a \in ByzAcceptor |->
                                           (VLVotingMessageInbox[a] \cup
                                                {[acc |-> self, vote |-> nextExpectedAction[self]]})]}:
                             /\ VLVotingMessageInbox' = newInbox
                             /\ VLVotingPhase' = [VLVotingPhase EXCEPT ![self] = 2]
                             /\ holdingRoomInterest' = [holdingRoomInterest EXCEPT ![self] = 0]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, nextAction, actionPerformed, nextExpectedAction, VLVotingMessageStorage, VLVotingCounter, VLActionHistory>>
                     \/ /\ /\ VLVotingPhase[self] = 2
                           /\ holdingRoomInterest[self] = -1
                           /\ VLVotingMessageInbox[self] # {}
                        /\ \E newInbox \in {[a \in ByzAcceptor |->
                                                (VLVotingMessageInbox[a] \cup
                                                 VLVotingMessageInbox[self])]}:
                             /\ VLVotingMessageStorage' = [VLVotingMessageStorage EXCEPT ![self] = VLVotingMessageStorage[self] \cup
                                                                                                   VLVotingMessageInbox[self]]
                             /\ VLVotingMessageInbox' = newInbox
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, holdingRoomInterest, nextAction, actionPerformed, nextExpectedAction, VLVotingPhase, VLVotingCounter, VLActionHistory>>
                     \/ /\ /\ VLVotingPhase[self] = 2
                           /\ holdingRoomInterest[self] = -1
                        /\ \E m \in VLVotingMessageInbox[self] \intersect
                                    VLVotingMessageStorage[self]:
                             VLVotingMessageInbox' = [VLVotingMessageInbox EXCEPT ![self] = VLVotingMessageInbox[self] \ {m}]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, holdingRoomInterest, nextAction, actionPerformed, nextExpectedAction, VLVotingPhase, VLVotingMessageStorage, VLVotingCounter, VLActionHistory>>
                     \/ /\ /\ VLVotingPhase[self] = 3
                           /\ holdingRoomInterest[self] = -1
                        /\ \E m \in {mm \in VLVotingMessageStorage[self] :
                                         /\ \A n \in VLVotingMessageStorage[self] :
                                              mm.vote = n.vote \/ mm.acc # n.acc
                                         /\ \A v \in VLVotingCounter[self] : v.vote # mm.vote}:
                             /\ VLVotingMessageStorage' = [VLVotingMessageStorage EXCEPT ![self] = VLVotingMessageStorage[self] \ {m}]
                             /\ VLVotingCounter' = [VLVotingCounter EXCEPT ![self] = VLVotingCounter[self] \cup
                                                                                     {[vote |-> m.vote, count |-> 1]}]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, holdingRoomInterest, nextAction, actionPerformed, nextExpectedAction, VLVotingPhase, VLVotingMessageInbox, VLActionHistory>>
                     \/ /\ /\ VLVotingPhase[self] = 3
                           /\ holdingRoomInterest[self] = -1
                        /\ \E m \in {mm \in VLVotingMessageStorage[self] :
                                         \A n \in VLVotingMessageStorage[self] :
                                              mm.vote = n.vote \/ mm.acc # n.acc}:
                             \E v \in {vv \in VLVotingCounter[self] : vv.vote = m.vote}:
                               /\ VLVotingMessageStorage' = [VLVotingMessageStorage EXCEPT ![self] = VLVotingMessageStorage[self] \ {m}]
                               /\ VLVotingCounter' = [VLVotingCounter EXCEPT ![self] = (VLVotingCounter[self] \ {v}) \cup
                                                                                       {[vote |-> v.vote, count |-> v.count + 1]}]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, holdingRoomInterest, nextAction, actionPerformed, nextExpectedAction, VLVotingPhase, VLVotingMessageInbox, VLActionHistory>>
                     \/ /\ /\ VLVotingPhase[self] = 3
                           /\ holdingRoomInterest[self] = -1
                           /\ \A m \in VLVotingMessageStorage[self]:
                                \E n \in VLVotingMessageStorage[self]:
                                     n.acc = m.acc /\ n.vote # m.vote
                        /\ \E m \in {mm \in VLVotingCounter[self]:
                                         \A n \in VLVotingCounter[self] :
                                              mm.count >= n.count /\
                                             (mm.count = n.count =>
                                                  (mm.vote.bal <= n.vote.bal /\
                                                  (mm.vote.bal = n.vote.bal =>
                                                       (mm.vote.type = "1c" /\
                                                       (n.vote.type = "1c" =>
                                                            mm.vote.val <= n.vote.val)))))}:
                             /\ nextAction' = [nextAction EXCEPT ![self] = m.vote]
                             /\ nextExpectedAction' = [nextExpectedAction EXCEPT ![self] = [type |-> "None"]]
                             /\ VLActionHistory' = (VLActionHistory \cup {m.vote})
                             /\ actionPerformed' = [actionPerformed EXCEPT ![self] = 0]
                             /\ VLVotingPhase' = [VLVotingPhase EXCEPT ![self] = 0]
                        /\ UNCHANGED <<maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, holdingRoomInterest, VLVotingMessageInbox, VLVotingMessageStorage, VLVotingCounter>>
                  /\ UNCHANGED holdingRoomOpen

sync == /\ \/ /\ \A a \in Acceptor: nextAction[a].type = "None"
              /\ nextAction' = [a \in Acceptor |-> [type |-> "1a", bal |-> 0]]
              /\ VLActionHistory' = (VLActionHistory \cup {[type |-> "1a", bal |-> 0]})
              /\ UNCHANGED <<holdingRoomOpen, VLVotingPhase>>
           \/ /\ /\ holdingRoomOpen = 0
                 /\ \A a \in Acceptor : holdingRoomInterest[a] # 1
              /\ holdingRoomOpen' = 1
              /\ UNCHANGED <<nextAction, VLVotingPhase, VLActionHistory>>
           \/ /\ /\ holdingRoomOpen = 1
                 /\ \A a \in Acceptor : holdingRoomInterest[a] = 1
              /\ holdingRoomOpen' = 0
              /\ UNCHANGED <<nextAction, VLVotingPhase, VLActionHistory>>
           \/ /\ /\ \A a \in Acceptor: VLVotingPhase[a] = 2
                 /\ \A a \in Acceptor: VLVotingMessageInbox[a] = {}
              /\ VLVotingPhase' = [a \in Acceptor |-> 3]
              /\ UNCHANGED <<holdingRoomOpen, nextAction, VLActionHistory>>
        /\ UNCHANGED << maxBal, maxVBal, maxVVal, 2avSent, knowsSent, vlbmsgs, 
                        holdingRoomInterest, actionPerformed, 
                        nextExpectedAction, VLVotingMessageInbox, 
                        VLVotingMessageStorage, VLVotingCounter >>

facceptor(self) == /\ \/ /\ \E m \in { mm \in 1bMessage \cup 2avMessage \cup 2bMessage :
                                       mm.acc = self}:
                              vlbmsgs' = (vlbmsgs \cup {m})
                         /\ UNCHANGED VLVotingMessageInbox
                      \/ /\ \E a \in Acceptor:
                              \E m \in  {mm \in 1aMessage \cup 1cMessage :
                                       /\ (mm \in 1aMessage => mm.bal = maxBal[a] + 1)
                                       /\ (mm \in 1cMessage => mm.bal = maxBal[a])}:
                                VLVotingMessageInbox' = [VLVotingMessageInbox EXCEPT ![a] = VLVotingMessageInbox[a] \cup {[acc |-> self, vote |-> m ]}]
                         /\ UNCHANGED vlbmsgs
                   /\ UNCHANGED << maxBal, maxVBal, maxVVal, 2avSent, 
                                   knowsSent, holdingRoomInterest, 
                                   holdingRoomOpen, nextAction, 
                                   actionPerformed, nextExpectedAction, 
                                   VLVotingPhase, VLVotingMessageStorage, 
                                   VLVotingCounter, VLActionHistory >>

Next == sync
           \/ (\E self \in Acceptor: acceptor(self))
           \/ (\E self \in FakeAcceptor: facceptor(self))

Spec == Init /\ [][Next]_vars

\* END TRANSLATION
-----------------------------------------------------------------------------
 
 
 
MC_acceptor == {-11, -12, -13}

MC_fakeacceptor == {-101}

MC_value == {1, 2}

MC_byzquorum == {{-11, -12}, {-11, -13}, {-12, -13}, {-11, -12, -13},
{-11, -12, -101}, {-11, -13, -101}, {-12, -13, -101}, {-11, -12, -13, -101}}

MC_weakquorum == {{-11, -12}, {-11, -13}, {-12, -13}, {-11, -12, -13},
{-11, -12, -101}, {-11, -13, -101}, {-12, -13, -101}, {-11, -12, -13, -101},
{-11}, {-12}, {-13}, {-11, -101}, {-12, -101}, {-13, -101}}



MC_inv_next_expected_action_filled == \A a \in Acceptor : 
   VLVotingPhase[a] # 0 => nextExpectedAction[a].type # "None"
   

MC_inv_phase1_successful_broadcast == \A a \in Acceptor:
  (VLVotingPhase[a] = 2 /\ holdingRoomInterest[a] = 0) =>
     (\A b \in Acceptor: 
        [acc |-> a, vote |-> nextExpectedAction[a]] 
             \in VLVotingMessageInbox[b])
             
             
MC_inv_phase2_equivalent_storage ==  \A a, b \in {aa \in Acceptor: 
               VLVotingPhase[aa] = 2
            /\ VLVotingMessageInbox[aa] = {}}:
    VLVotingMessageStorage[a] = VLVotingMessageStorage[b]             

MC_inv_equivalent_next_action ==  \A a, b \in Acceptor:
  (actionPerformed[a]  = 0 /\ actionPerformed[b] = 0) => 
    nextAction[a] = nextAction[b]
 
(*Model start to phase 1*)

MC_start_to_phase1_state_constraint == 
       (\A m \in vlbmsgs : m.acc # -101)
    /\ (\A a \in Acceptor:
           \A m \in VLVotingMessageInbox[a]:
              m.acc # -101)
    /\ holdingRoomOpen = 1

MC_start_to_phase1_action_constraint ==
    (knowsSent' # knowsSent) => 
        \E a \in Acceptor:
            \E b \in Ballot : 
                knowsSent[a]' # knowsSent[a]
             /\ knowsSent[a]' = knowsSent[a] \cup sentMsgs("1b", b)

(*Model phase 1 to phase 3*)
 
MC_phase1_to_phase3_state_constraint == 
    (\A m \in vlbmsgs : m.acc # -101)
 /\ (\A a \in Acceptor:
        \A m \in VLVotingMessageInbox[a]:
            m.acc # -101)
 
MC_phase1_to_phase3_action_constraint == 
    ((knowsSent' # knowsSent) => 
        \E a \in Acceptor:
            \E b \in Ballot : 
                knowsSent[a]' # knowsSent[a]
             /\ knowsSent[a]' = knowsSent[a] \cup sentMsgs("1b", b))
 /\ vlbmsgs' = vlbmsgs

(*
MC_phase1_to_phase3_init ==
/\  2avSent = (-13 :> {} @@ -12 :> {} @@ -11 :> {})
/\  vlbmsgs = { [ acc |-> -13,
    type |-> "1b",
    bal |-> 0,
    mbal |-> -1,
    mval |-> None,
    m2av |-> {} ],
  [ acc |-> -12,
    type |-> "1b",
    bal |-> 0,
    mbal |-> -1,
    mval |-> None,
    m2av |-> {} ],
  [ acc |-> -11,
    type |-> "1b",
    bal |-> 0,
    mbal |-> -1,
    mval |-> None,
    m2av |-> {} ] }
/\  holdingRoomInterest = (-13 :> 1 @@ -12 :> 1 @@ -11 :> 1)
/\  maxVBal = (-13 :> -1 @@ -12 :> -1 @@ -11 :> -1)
/\  holdingRoomOpen = 0
/\  nextAction = ( -13 :> [type |-> "1a", bal |-> 0] @@
  -12 :> [type |-> "1a", bal |-> 0] @@
  -11 :> [type |-> "1a", bal |-> 0] )
/\  VLVotingCounter = (-13 :> {} @@ -12 :> {} @@ -11 :> {})
/\  VLVotingPhase = (-13 :> 1 @@ -12 :> 1 @@ -11 :> 1)
/\  maxVVal = (-13 :> None @@ -12 :> None @@ -11 :> None)
/\  nextExpectedAction = ( -13 :> [type |-> "1a", bal |-> 1] @@
  -12 :> [type |-> "1c", bal |-> 0, val |-> 1] @@
  -11 :> [type |-> "1c", bal |-> 0, val |-> 1] )
/\  actionPerformed = (-13 :> 1 @@ -12 :> 1 @@ -11 :> 1)
/\  VLVotingMessageStorage = (-101 :> {} @@ -13 :> {} @@ -12 :> {} @@ -11 :> {})
/\  VLVotingMessageInbox = (-101 :> {} @@ -13 :> {} @@ -12 :> {} @@ -11 :> {})
/\  knowsSent = ( -13 :> {} @@
  -12 :>
      { [ acc |-> -13,
          type |-> "1b",
          bal |-> 0,
          mbal |-> -1,
          mval |-> None,
          m2av |-> {} ],
        [ acc |-> -12,
          type |-> "1b",
          bal |-> 0,
          mbal |-> -1,
          mval |-> None,
          m2av |-> {} ] } @@
  -11 :>
      { [ acc |-> -13,
          type |-> "1b",
          bal |-> 0,
          mbal |-> -1,
          mval |-> None,
          m2av |-> {} ],
        [ acc |-> -12,
          type |-> "1b",
          bal |-> 0,
          mbal |-> -1,
          mval |-> None,
          m2av |-> {} ] } )
/\  maxBal = (-13 :> 0 @@ -12 :> 0 @@ -11 :> 0)
/\ VLActionHistory = {[type |-> "1a", bal |-> 0]}
*)
 
(* Refinement Invariant *)
Inv1 == \A a \in Acceptor: nextAction[a].type = "None"  
                        \/ nextAction[a] \in VLActionHistory

Inv2 == (\A a \in Acceptor : nextAction[a].type = "None") 
            => VLActionHistory = {}

Inv3 == \A m \in vlbmsgs : m.type # "1a" /\ m.type # "1c"

Inv4 == \A a \in Acceptor : 
          \A m \in VLActionHistory :
            ((nextAction[a].type # "None"
              => nextAction[a].bal >= m.bal)
         /\ (nextExpectedAction[a].type  # "None"
              => nextExpectedAction[a].bal >= m.bal)
         /\ (\A mm \in VLVotingMessageInbox[a] : 
               mm.vote.bal >= m.bal)
         /\ (\A mm \in VLVotingMessageStorage[a] :
               mm.vote.bal >= m.bal)
         /\ (\A mm \in VLVotingCounter[a] :
               mm.vote.bal >= m.bal))      

Inv5 == \A a \in Acceptor:
          \A m \in VLActionHistory:
            actionPerformed[a] = 1
              => maxBal[a] >= m.bal
              
Inv6 == \A a \in Acceptor:
          nextExpectedAction[a].type = "1c" =>
             (([type |-> "1a", bal |-> nextExpectedAction[a].bal]
               \in VLActionHistory)
          /\ (\A m \in VLActionHistory:
                m.type # "1c"  
             \/ m.val = nextExpectedAction[a].val 
             \/ m.bal # nextExpectedAction[a].bal)
          /\ (KnowsSafeAt(a, 
                          nextExpectedAction[a].bal,
                          nextExpectedAction.bal)))
              
f == Cardinality(FakeAcceptor)

Inv7 == \A a \in Acceptor:
          \A v \in VLVotingCounter[a]:
            v.count > f =>
              (\E b \in Acceptor:
                nextExpectedAction[b] = v.vote)
             \/ v.vote \in VLActionHistory
             

Inv == Inv1 /\ Inv2 /\ Inv3 /\ Inv4 /\ Inv5 /\ Inv6 /\ Inv7

  
 
 
 
 
 
==============================================================================
\* Modification History
\* Last modified Tue Oct 27 17:37:06 EST 2015 by Navin
\* Last modified Tue Feb 08 11:53:20 PST 2011 by lamport
